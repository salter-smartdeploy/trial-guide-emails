'use strict';

const gulp = require('gulp'),
    inlineCSS = require('gulp-inline-css'),
    htmlmin = require('gulp-htmlmin'),
    runSequence = require('run-sequence'),
    replace = require('gulp-replace'),
    settings = require('./settings.json');

gulp.task('inlineCSS', function() {
    return gulp.src(settings.paths.html)
        .pipe(inlineCSS(settings.inlineCSS))
        // Replace is finding and replacing makeshift token pieces because
        // the inliner breaks when there are Marketo tokens in the style tags :(
            .pipe(replace('asdf-check-high', '${checkbox_highlighted_'))
            .pipe(replace('asdf-check-img', '${checkbox_image_'))
            .pipe(replace('asdf-text-white', '${checkbox_white_text_'))
            .pipe(replace('asdf-close-braket', '}'))
        .pipe(htmlmin(settings.htmlmin))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('watch', function (){
    gulp.watch(settings.paths.css, ['inlineCSS']);
    gulp.watch(settings.paths.html, ['inlineCSS']);
});

gulp.task('default', function (callback) {
    runSequence('inlineCSS', 'watch', callback)
});