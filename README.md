## SmartDeploy Email Dev

### BUG 1: Fonts

You have to manually re-add the font link tab to /dist/index.html :( The inliner strips it out.
```html
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">

```

### BUG 2: CSS Inliner and Marketo Tokens

Don't remove any styles starting with "asdf" in the html files.

The inliner can't handle Marketo tokens in the style tags like `style="background: ${checkbox_highlighted_1}"` so there's a gulp `replace` task using the asdf styles for a find and replace. See the gulpfile.

### BUG 3: Marketo Module Gap

Couldn't keep each checklist item as a separet marketo module because outlook shows random gaps between the gray backgrounds. The gap jumps around depending on what monitor my email window was open on or what my zoom was set to (╯°□°）╯︵ ┻━┻ Even the marketo templates have the bug. That's why it's set to one big, messy module.

### Getting Started

1. Install [Node.js with npm](https://nodejs.org/en/) and [Gulp.js](http://gulpjs.com/)
2. Open a command line tool and navigate to this directory
3. Run `npm install` to download all the required node modules
4. Run `gulp` to start watching your files for changes, or `gulp thetaskname` to run a single task

### Gulp Tasks

| Task Name  |  Task Purpose |
|---|---|
| `gulp inlineCSS`  |  Create a new html file with all non-media query CSS inlined  |

### Styles

Styles are based off of Foundation's [Ink Templates](https://foundation.zurb.com/emails/email-templates.html)

Some styles like *Media Queries and hover/focus states need to go in the src/html files*.  
The CSS inliner task can't inject them if they come from a linked CSS file. See this [inliner media query issue](https://github.com/jonkemp/gulp-inline-css/issues/23).

#### How CSS is Inlined

If you have two conflicting styles in app.css, gulp inlineCSS will only write one of them into dist/html.

So if you have this:

```css
.red-text { color: red }
.blue-text { color: blue }

p.large-text { font-size: 20px }
.small-text { font-size: 12px }
```

The blue text is inlined because its class comes *after* the red class in app.css  
But the large text gets inlined because it has greater specificity than the small text class

```html
<p class="blue-text red-text large-text small-text" styles="color:blue; font-size: 20px">Pika pika!</p>
```

The order you write classes into `<p>` doesn't matter.
